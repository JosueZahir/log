package com.example.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {

    private EditText editTextNumero;
    private EditText editTextExplorador;
    private ImageButton botonNumero;
    private ImageButton botonExplorador;

    private final int PHONE_CALL_CODE = 100;
    private final int CAMERA_CALL_CODE = 120; //Puede que no sea el código de la acamara
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        editTextNumero = (EditText) findViewById(R.id.NumeroMarcar);
        //editTextExplorador = (EditText) findViewById(R.id.explorador);

        botonNumero = (ImageButton) findViewById(R.id.btnNumero);
        //botonExplorador = (ImageButton) findViewById(R.id.btnExplorador);

        botonNumero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = editTextNumero.getText().toString();
                if (num != null)
                {
                    if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
                    {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }else {
                        versionesAnteriores(num);
                    }
                }
            }

            private void versionesAnteriores (String num)
            {
                Intent intentLlamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+ num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE))
                {
                    startActivity(intentLlamada);
                }
                else {
                    Toast.makeText(MainActivity2.this, "Configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if(permission.equals(Manifest.permission.CALL_PHONE)){
                    //Comprueba si el permiso es aceptado o nel
                    if (result == PackageManager.PERMISSION_GRANTED){
                        //Que siempre si puede marcar dice :v
                        String phoneNumber = editTextNumero.getText().toString();
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber));
                        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ) return;
                        startActivity(llamada);
                    }
                    else{
                        //que nel no puedes marcar dice
                        Toast.makeText(this, "No aceptaste el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
        }
    }

    private boolean verificarPermisos(String permiso)
    {
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado == PackageManager.PERMISSION_GRANTED;
    }
}